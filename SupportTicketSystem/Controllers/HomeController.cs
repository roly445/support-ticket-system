﻿namespace ExceptionGenerator.Controllers
{
	using System.Web.Mvc;
	using NLog;

	public class HomeController : Controller
	{
		private static readonly Logger logger = LogManager.GetCurrentClassLogger();

		public ActionResult Index()
		{
			logger.Error("My Error");

			return View();
		}
	}
}