﻿CREATE TABLE [dbo].[Log]
(
	[Id] int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	[TimeStamp] datetime NOT NULL DEFAULT GETDATE(),
	[Host] nvarchar(MAX) NOT NULL,
	[Type] nvarchar(200) NOT NULL,
	[Source] nvarchar(200) NOT NULL,
	[Message] nvarchar(MAX) NOT NULL,
	[Level] nvarchar(50) NOT NULL,
	[Logger] nvarchar(200) NOT NULL,
	[Stacktrace] nvarchar(MAX) NULL
)
